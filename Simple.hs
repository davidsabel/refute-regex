 -- module SimpleReimplement where

import RegExpr.RegExprOperations
import Control.Monad
import Data.List
import System.Environment 
import System.IO
import Data.IORef
import qualified Data.Set as Set

-- =================================================================================
-- boring utility-Functions:         
splits []  = []     
splits [x] = [([],x,[])]    
splits (x:xs) = 
 ([],x,xs):[(b++[x],a,ys) | (b,a,ys) <- splits xs]

fnub :: Ord a => [a] -> [a]         
fnub xs = fastNubBy xs (Set.empty)  

fastNubBy :: Ord a => [a] -> Set.Set a -> [a]
fastNubBy [] _ = []
fastNubBy (x:xs) set 
    | Set.member x set = fastNubBy xs set
    | otherwise        = x:(fastNubBy xs (Set.insert x set))

fst3 :: (a,b,c) -> a
fst3 (a,_,_) = a
-- =================================================================================
-- PISIMPLE
-- =================================================================================
data PISIMPLE = Zero | One | Out PISIMPLE | In PISIMPLE
 deriving(Eq,Ord)
 
sortPISIMPLEProc (PISIMPLEProc xs) = PISIMPLEProc  (simplify $ sort xs)
 
simplify (Zero:Zero:xs) = (simplify (Zero:xs)) 
simplify (Zero:One:xs) = (simplify (One:xs))
simplify (One:One:xs) = (simplify (One:xs))
simplify xs = xs

instance Show PISIMPLE where
    show (Zero) = "0"
    show (One) = "1"
    show (Out x) = "!"++(show x)
    show (In x) = "?" ++(show x)

newtype PISIMPLEProc = PISIMPLEProc [PISIMPLE]
 deriving(Eq,Ord)
instance Show PISIMPLEProc where
    show (PISIMPLEProc xs) = intercalate " | " (map show xs)

successful (PISIMPLEProc proc) = 
    One `elem` proc
    
isOne One = True
isOne _ = False
isZero Zero = True
isZero _ = False
isOut (Out _) = True
isOut _ = False
isIn (In _) = True
isIn _ = False

oneSteps :: PISIMPLEProc -> [PISIMPLEProc]
oneSteps (PISIMPLEProc proc) = 
    let ones  = case filter isOne proc of 
                 One:_ -> [One]
                 [] -> []
        outs = filter isOut proc
        ins = filter isIn proc
        pot = [ones ++ [r1,r2] ++ b1 ++ b2 ++ a1 ++ a2 | (b1,Out r1,a1) <- splits outs, (b2,In r2,a2) <- splits ins] 
    in map PISIMPLEProc $ Set.elems (Set.fromList [(sort a) | a <- pot])
  

-- checking may-convergence and should-convergence for PISIMPLE-processes
maycon :: PISIMPLEProc -> Bool
maycon (PISIMPLEProc []) = False
maycon xs = case oneSteps xs of 
             [] -> successful xs
             yys -> any maycon yys
shouldcon :: PISIMPLEProc -> Bool
shouldcon xs = maycon xs && all shouldcon (oneSteps xs)



-- ======================================
-- CHSIMPLE
-- ======================================

data CHSimpleAction = Stop | Rec | Send | Take | Put
                     -- additional constructors for regular expressions:
                     | More | PTPlus  | TPPlus  | PTStar | TPStar | Alt [[CHSimpleAction]]
 deriving(Eq,Ord,Show)

newtype ShowCH = ShowCH CHSimpleAction
instance Show ShowCH where
 show (ShowCH act) = dshow act
  where 
   dshow :: CHSimpleAction -> String
   dshow Stop = "1"
   dshow Rec = "R" 
   dshow Send = "S" 
   dshow Take = "T" 
   dshow Put = "P" 
   dshow More = "M" 
   dshow PTPlus = "(PT)+"
   dshow TPPlus = "(TP)+"
   dshow PTStar = "(PT)*"
   dshow TPStar = "(TP)*"
   dshow (Alt list) = intercalate "|" $ map (\actlist -> "[" ++ (intercalate "," (map dshow actlist)) ++ "]") list
   
type CHSIMPLEProcess = [CHSimpleAction]
type CHSIMPLEProgram = [CHSIMPLEProcess]

-- type CHSIMPLEProc = [CHSIMPLE]
type Msg = Bool
type Ack = Bool
type CHSIMPLEState = (CHSIMPLEProgram,Msg,Ack)
isStop Stop = True
isStop _ = False
isRec (Rec) = True
isRec _  = False
isSend (Send ) =True
isSend _ = False
isTake (Take ) = True
isTake _ = False
isPut (Put ) = True
isPut _ = False
isMore More = True
isMore _ = False
isPTPlus PTPlus = True
isPTPlus _ = False
isTPPlus TPPlus = True
isTPPlus _ = False
isPTStar PTStar = True
isPTStar _ = False
isTPStar TPStar = True
isTPStar _ = False
isAlt (Alt _) = True
isAlt _ = False
initState xs = (xs,False,False)

onHead p (x:xs) = p x
onHead _ _ = False
    
-- only for concrete processes:    
oneStepsCH :: CHSIMPLEState -> [CHSIMPLEState]
oneStepsCH (proc,msg,ack) = 
    let 
      ones = case filter (onHead isStop) proc of
               _:_ -> [[Stop]]
               []     -> []
              
      rs = filter (onHead isRec) proc
      ss = filter (onHead isSend) proc
      ts = filter (onHead isTake) proc
      ps = filter (onHead isPut) proc
      pot =  [(ones ++ ss ++ ts ++ ps ++ [a] ++ bef ++ after,False,ack) | (bef,Rec:a,after) <- splits rs, msg]
           ++ [(ones ++ rs ++ ts ++ ps ++ [a] ++ bef ++ after,True,ack)  | (bef,Send:a,after) <- splits ss, not msg]
           ++ [(ones ++ rs ++ ss ++ ps ++ [a] ++ bef ++ after,msg,False) | (bef,Take:a,after) <- splits ts, ack]
           ++ [(ones ++ rs ++ ss ++ ts ++ [a] ++ bef ++ after,msg,True)  | (bef,Put:a,after) <- splits ps, not ack]
    in Set.elems (Set.fromList [(sort a,b,c) | (a,b,c) <- pot])

successfulCH (proc,_,_) = any (onHead isStop) proc

-- for pattern-processes
mayconCH :: CHSIMPLEProgram -> Bool
mayconCH xs = mayconCH' (initState xs)

mayconCH' :: CHSIMPLEState -> Bool
mayconCH' ([],_,_) = False
mayconCH' xs = case oneStepsCH xs of 
               [] -> successfulCH xs
               yys -> any mayconCH' yys
               
shouldconCH :: CHSIMPLEProgram -> Bool               
shouldconCH xs = shouldconCH' (initState xs)
shouldconCH' :: CHSIMPLEState -> Bool
shouldconCH' xs = mayconCH' xs && all shouldconCH' (oneStepsCH xs)
    
oneStepsCHWithPatterns :: CHSIMPLEState -> [CHSIMPLEState]
oneStepsCHWithPatterns (proc,msg,ack) = 
    let 
      ones = case filter (onHead isStop) proc of
               _:_ -> [[Stop]]
               []     -> []
              
      rs = filter (onHead isRec) proc
      ss = filter (onHead isSend) proc
      ts = filter (onHead isTake) proc
      ps = filter (onHead isPut) proc
      ms = filter (onHead isMore) proc
      pts = filter (onHead isPTStar) proc
      ptp = filter (onHead isPTPlus) proc
      tps = filter (onHead isTPStar) proc
      tpp = filter (onHead isTPPlus) proc
      alts = filter (onHead isAlt) proc
      
      pot =   [((concat [ones,--rs,
                            ss,ts,ps,ms,pts,ptp,tps,tpp,alts]) ++
                            [a] ++ bef ++ after,False,ack) | (bef,Rec:a,after) <- splits rs, msg]
           ++ [((concat [ones,rs,-- ss,
                             ts,ps,ms,pts,ptp,tps,tpp,alts]) ++
                            [a] ++ bef ++ after,True,ack)  | (bef,Send:a,after) <- splits ss, not msg]
           ++ [((concat [ones,rs,ss,-- ts,
                                ps,ms,pts,ptp,tps,tpp,alts]) ++
                            [a] ++ bef ++ after,msg,False) | (bef,Take:a,after) <- splits ts, ack]
           ++ [((concat [ones,rs,ss,ts,-- ps,
                                      ms,pts,ptp,tps,tpp,alts]) ++
                            [a] ++ bef ++ after,msg,True)  | (bef,Put:a,after) <- splits ps, not ack]
           ++ (concat
               [[((concat [ones,rs,ss,ts,ps,ms,pts,--ptp,
                                               tps,tpp,alts]) ++
                            [a] ++ bef ++ after,msg,False),
                ((concat [ones,rs,ss,ts,ps,ms,pts,--ptp
                                               tps,tpp,alts]) ++
                            [Take:a] ++ bef ++ after,msg,True)
                ] | (bef,PTPlus:a,after) <- splits ptp, not ack])
           ++ (concat
               [[((concat [ones,rs,ss,ts,ps,ms,pts,ptp,tps,--tpp,
                                                        alts]) ++
                            [a] ++ bef ++ after,msg,True),
                ((concat [ones,rs,ss,ts,ps,ms,pts,ptp,tps,--tpp,
                                                        alts]) ++
                            [Put:a] ++ bef ++ after,msg,False)
                ] | (bef,TPPlus:a,after) <- splits tpp, ack])
    in Set.elems (Set.fromList [(sort a,b,c) | (a,b,c) <- pot])

data EvalTree a b = Node a [EvalTree a b] | Leaf b
 deriving (Show)

genEvalTree :: (a -> [a]) -> (a -> Bool) -> (a -> Bool) -> a -> EvalTree a (a,String)
genEvalTree successorFn successfulFn mustDivFn expr 
 | successfulFn expr = Leaf (expr,"successful")
 | mustDivFn expr    = Leaf (expr,"must-div")
 | otherwise = Node expr (map (genEvalTree successorFn successfulFn mustDivFn) (successorFn expr))

evalTreeCH :: CHSIMPLEState -> EvalTree CHSIMPLEState (CHSIMPLEState,String)
evalTreeCH expr = genEvalTree oneStepsCHWithPatterns successfulCH incompleteMustdivCHPat expr
showPath path = intercalate "\n -> " $ map showStateItem path 
showStateItem (Left (prg,m,ack)) = showState (prg,m,ack)
showStateItem (Right ((prg,m,ack),string)) = (showState (prg,m,ack)) ++ " " ++ string 
showState (prg,m,ack) = "(" ++ show (map (map ShowCH) prg) ++ "," ++ (if m then "\11035" else "\8709") ++ "," ++  (if ack then "\11035" else "\8709") ++ ")"
getPathes (Leaf b)    = [[Right b]]
getPathes (Node a xs) = [(Left a):path | path <- concatMap getPathes xs]

mayconPathes pathes = filter (\p -> case last p of 
                                       (Right (_,"successful")) -> True  
                                       _ -> False
                             )
                             pathes  
maydivPathes pathes = filter (\p -> case last p of 
                                       (Right (_,"must-div")) -> True  
                                       _ -> False
                             )
                             pathes
evidence process = intercalate "\n" [evidenceMaycon process,evidenceMaydiv process]                               
evidenceMaycon process =  concatMap showPath $ take 1 $ mayconPathes $ getPathes $ evalTreeCH (initState process)
evidenceMaydiv process =  concatMap showPath $ take 1 $ maydivPathes $ getPathes $ evalTreeCH (initState process)


-- for Patterns

incompleteMayconCHPat :: CHSIMPLEState -> Bool
incompleteMayconCHPat ([],_,_) = False
incompleteMayconCHPat xs = case oneStepsCHWithPatterns xs of 
                    [] -> successfulCH xs
                    yys -> any incompleteMayconCHPat yys

incompleteMustdivCHPat :: CHSIMPLEState -> Bool 
incompleteMustdivCHPat xs@(a,_,_) = case oneStepsCHWithPatterns xs of
                             [] -> all (\y -> null y  || onHead (not . isStop) y) a 
                                   && all (\y -> null y || onHead (\y -> not (isTPStar y|| isMore y || isPTStar y || isAlt y)) y) a
                             _ -> False      
incompleteMaydivPat st = incompleteMustdivCHPat st || 
                            (any incompleteMaydivPat (oneStepsCHWithPatterns st))
            
patIncompleteMayDiv = incompleteMaydivPat . initState
patIncompleteMayCon = incompleteMayconCHPat . initState

-- Translations

type Translation = (CHSIMPLEProcess,CHSIMPLEProcess)
sampletrans :: Translation
sampletrans   = ([Send,Take,Put],[Rec,Put,Take])
sampletrans1  = ([Send,Put,Put],[Rec,Take,Take])

applyTrans :: Translation -> PISIMPLEProc -> CHSIMPLEProgram

applyTrans t (PISIMPLEProc xs) = map (applyTransOne t) xs

applyTransOne :: Translation -> PISIMPLE -> CHSIMPLEProcess
applyTransOne t One = [Stop]
applyTransOne t Zero = []
applyTransOne t@(send,recv) (Out r) = 
     let r' = applyTransOne t r
     in  (send) ++ r'
applyTransOne t@(send,recv) (In r) = 
     let r' = applyTransOne t r
     in  (recv) ++ r'


validPat (p,tp) = not (inValidPat (p,tp))

inValidPat (p,tp) = 
      (shouldcon p && (patIncompleteMayDiv tp) )
    || (not (maycon p)  &&  (patIncompleteMayCon tp))

valid (p,tp) = shouldcon p == shouldconCH tp && maycon p == mayconCH tp

validString (p,tp) = (if maycon p then "p maycon" else "p not maycon") ++ ","
                     ++ (if mayconCH tp then "t(p) maycon" else "t(p) not maycon") ++ ","
                     ++ (if shouldcon p then "p shouldcon" else "p not shouldcon") ++ ","
                     ++ (if shouldconCH tp then "t(p) shouldcon" else "t(p) not shouldcon") 

validStringPat (p,tp) = (if (shouldcon p && patIncompleteMayDiv tp) then "p shouldcon but t(p) maydiv" else "")
                     ++ (if (not (maycon p) && patIncompleteMayCon tp) then "p mustdiv but t(p) maycon" else "")
                     
checkAll samplePrograms translation = 
  let tests  = [(p,applyTrans translation p) | p <- samplePrograms]
  in all valid tests
  
checkAllPred predicate samplePrograms translation  = 
  let tests  = [(p,applyTrans translation p) | p <- samplePrograms]
  in  (all predicate tests)

  
checkTranslations validStringFn predicate flag samplePrograms ioref count [] =
           return()
          
checkTranslations validStringFn predicate flag samplePrograms ioref count (t:translations) =
    do
      hSetBuffering stdout NoBuffering
      (con,can) <- readIORef ioref
      let sampleC = Set.elems con
      let samplePrograms' = sampleC ++ samplePrograms
      -- hPutStr stderr ((show count) ++ " ")
      if flag > 0 then putStrLn $ "Checking Translation " ++ show t  else return ()
      if (checkAllPred predicate samplePrograms' t) then 
       do 
         putStrLn $ "  Translation " ++ show t ++ " may be correct?"
         putStrLn $ "  Did not find a counter-example!"
         let can'=Set.insert t can 
         writeIORef ioref (con,can')
         checkTranslations validStringFn predicate flag samplePrograms ioref (count+1) translations
        else do 
        -- hPutStr stderr ("" ++ replicate ((2+length ((show count)))) ' ' )
         let tests  = [(predicate (p,applyTrans t p),(p,applyTrans t p,validStringFn (p,applyTrans t p))) | p <- samplePrograms']
         let ex = ( head $ filter (\(a,b) -> a == False) tests )
         refc <- readIORef ioref
         (c1,c2) <-readIORef ioref
         writeIORef ioref (Set.insert (fst3 $ snd  ex) c1,c2)
         if flag > 0 then do
          putStrLn $ "  Translation " ++ show t ++ " refuted:"
          let (_,(cex,tp,string)) = ex 
          putStrLn $ "  Counter-example: p    = " ++ (show cex)                 
          putStrLn $ "                   t(p) = " ++  show tp
          putStrLn $ "                   Then: " ++ string
          when (flag > 1) (putStrLn $ "                   Witness-reduction for t(p):")
          when (flag > 1) (putStrLn (unlines $ map (\x -> "                   " ++ x) $ lines $ evidence tp))
          checkTranslations validStringFn predicate flag samplePrograms ioref (count+1) translations
         else 
          checkTranslations validStringFn predicate flag samplePrograms ioref (count+1) translations
          


printTransLatex (send,receive) = 
  '(' : (printTransLaSingle send) ++ (',' : (printTransLaSingle receive) ++ ")")
printTransLaSingle list = concatMap printTransLaSingleR list
--  printTransLaSingleR [] = []
printTransLaSingleR Send = "S"
printTransLaSingleR Rec = "R"
printTransLaSingleR Put = "P"
printTransLaSingleR Take = "T"
printTransLaSingleR More = "M"
printTransLaSingleR PTStar = "(PT)^{*}"
printTransLaSingleR TPStar = "(TP)^{*}"
printTransLaSingleR PTPlus = "(PT)^{+}"
printTransLaSingleR TPPlus = "(TP)^{+}"

printTransLaSingleR (Alt altlist) = '(': (printTransLaSingleRAltA  altlist) ++ ")"
printTransLaSingleRAltA  altlist =  concat (intersperse "| "  (map printTransLaSingleRAltASin  altlist))
printTransLaSingleRAltASin [] = "\\lambda " 
printTransLaSingleRAltASin xs = printTransLaSingle xs

testPrintTrans = printTransLatex ([PTPlus,Put,Take,Send],[Rec,PTStar,Take,Put,Alt [[Put],[Take]]])


-- -------------------------------------------------------------------------------------
-- generation of PISIMPLE-processes up to a given size

genSamples :: Integer ->  [PISIMPLEProc]
genSamples k = map PISIMPLEProc $ concat [ (mapS ws) | j <- [1..k], ws <- distribute j ]
 where 
  samples 1 = [Zero,One]
  samples i = 
     let j = samples (i-1)
     in j ++ [In k | k <- j] ++ [Out k | k <- j]    
  distribute 0 = [[]]
  distribute m = [i:ks | i <- [1..m], ks <- distribute (m-i)]
  mapS []     = [[]]
  mapS (i:xs) = [proc:procs | proc <- samples i, procs <- mapS xs]
--
-- -------------------------------------------------------------------------------------


-- -------------------------------------------------------------------------------------
-- generation of translations up to a given size
translations :: Integer -> [Translation]
translations 0 = [([Send],[Rec])]
translations i = 
    let j = (translations (i-1))
    in  (nub [(x ++ send ++ y, x' ++ recv ++ y') | (send,recv) <- j, (x,y,x',y') <- combs])

combs = 
    [([Take,Put],[],[],[])
    ,([Put,Take],[],[],[])
    ,([],[Take,Put],[],[])
    ,([],[Put,Take],[],[])
    ,([],[],[Take,Put],[])
    ,([],[],[Put,Take],[])
    ,([],[],[],[Take,Put])
    ,([],[],[],[Put,Take])
    ,([Take],[Put],[],[])
    ,([Put],[Take],[],[])
    ,([Take],[],[Put],[])
    ,([Put],[],[Take],[])
    ,([Take],[],[],[Put])
    ,([Put],[],[],[Take])
    ,([],[Take],[Put],[])
    ,([],[Put],[Take],[])
    ,([],[Take],[],[Put])
    ,([],[Put],[],[Take])
    ,([],[],[Take],[Put])
    ,([],[],[Put],[Take])
   ]
  

-- different validities, may- and should-convergence, reflection, preservervation, equivalence
    
valid_mayequiv (p,tp) = maycon p == mayconCH tp
valid_shouldequiv (p,tp) = shouldcon p == shouldconCH tp 
valid_mayimpl (p,tp) = (not (maycon p)) || (mayconCH tp)
valid_shouldimpl (p,tp) = (not (shouldcon p)) || (shouldconCH tp) 
valid_mayrefl (p,tp) = ( (maycon p)) || (not $ mayconCH tp)
valid_shouldrefl (p,tp) = ( (shouldcon p)) || (not $ shouldconCH tp) 
 
-- refuteAndReturn takes a list of translations, a list of counter-examples and an verbosity-level (0,1,2)
-- it checks whether the translations are refutable by the given counter-examples
-- and prints information according to the verbosity level.
-- It return a list of non-refuted translations 
     
refuteAndReturn :: [Translation] -> [PISIMPLEProc] -> Integer -> IO [Translation]      
refuteAndReturn translationslist exampleslist verbositylevel =
    do
     let unfolded = [(s',r') | (s,r) <- translationslist, s' <- unfoldPat s, r' <- unfoldPat r]
     examplesioref <- newIORef (Set.empty,Set.empty)
     let predicate = validPat
     putStrLn (">>>>>>>>>>>>>") 
     checkTranslations   validStringPat 
                         predicate 
                         verbositylevel 
                         ((fnub $ map sortPISIMPLEProc $ exampleslist))
                         examplesioref 
                         0 
                         unfolded -- ( translationslist)
     putStrLn "=========================="
     putStrLn "=========================="
     (r,r0) <- readIORef examplesioref
     putStrLn "Summary"
     putStrLn "=========================="
     putStrLn ("Checked the translations:")
     mapM_ print [(( (map ShowCH) a), ((map ShowCH) b)) | (a,b) <- translationslist]
     putStrLn "=========================="
     putStrLn ("First step: Unfold (PT)* (TP)* and alternatives:")
     putStrLn "................................................."
     mapM_ print [(( (map ShowCH) a), ((map ShowCH) b)) | (a,b) <- unfolded]
     putStrLn "................................................."
     putStrLn ("After checking the unfolded translations we get the following results")
     putStrLn "--------------------------"
     putStrLn "Used counter-examples:"
     putStrLn "--------------------------"
     mapM_ print r
     putStrLn "--------------------------"
     putStrLn "Non-refuted Translations:"
     putStrLn "--------------------------"
     mapM_ print [(( (map ShowCH) a), ((map ShowCH) b)) | (a,b) <- Set.elems r0]
     putStrLn ".........................."
     putStrLn "In internal syntax (for copy & paste):"
     putStrLn ".........................."
     print $ Set.elems r0
     putStrLn ".........................."
     putStrLn "LaTeX-output:"
     putStrLn ".........................."
     putStrLn $ "[" ++ (intercalate "," $ map printTransLatex $ Set.elems r0)  ++ "]"
     return $ Set.elems r0


-- count generates all translations up to translation_maxlength
-- and tries to refute them by generates counter-examples and 
-- an enumeration of counter-examples up to depth  examples_depth
-- verbositylevel changes how much output is printed (0,1,2 are ok)
-- predicatestring checks which behavior should be tested 
-- (default is valid = convergence equivalence w.r.t. may and should-convergence)
count translation_maxlength examples_depth verbositylevel predicatestring = 
    do
     examplesioref <- newIORef (Set.empty,Set.empty)
     let predicate = case predicatestring of
                       "mayequiv" -> valid_mayequiv
                       "shouldequiv" -> valid_shouldequiv
                       "mayimpl" -> valid_mayimpl
                       "shouldimpl" -> valid_shouldimpl
                       "mayrefl" -> valid_mayrefl
                       "shouldrefl" -> valid_shouldrefl
                       "valid" -> valid
                       _ -> valid

     sequence_ [putStrLn (">>>>>> " ++ show i ++ " >>>>>>>") 
                >> checkTranslations
                         validString 
                         predicate 
                         (verbositylevel) 
                         ((fnub $ map sortPISIMPLEProc $ (examplesInput ++ (genSamples examples_depth) )))
                         examplesioref 
                         0 
                         (translations i) 
                    | i <- [0..translation_maxlength]]
     putStrLn "=========================="
     (r,r0) <- readIORef examplesioref
     putStrLn "Summary"
     putStrLn "=========================="
     putStrLn ("Checked all translations with length up to " ++ show (2+2*translation_maxlength))
     putStrLn ("Bound on size for searching counter-examples " ++ show examples_depth)
     putStrLn "--------------------------"
     putStrLn "Used counter-examples:"
     putStrLn "--------------------------"
     mapM_ print r
     putStrLn "--------------------------"
     putStrLn "Non-refuted Translations:"
     putStrLn "--------------------------"
     mapM_ print r0





-- find unrefuted translations takes a list of translations,
-- a list of PISIMPLE-process and tests the translations.
-- it returns the translations that were not refutable
-- (and prints proofs for the refutations)

-- findUnref :: [Translation] -> [PISIMPLEProc] -> IO [Translation]
findUnref xs ys =  refuteAndReturn xs ys 2


-- Test-sets (PISIMPLE-processes)

criticalProcsFlat = map PISIMPLEProc
   [[Out One,In Zero]               -- !1 | ?0
   ,[Out One]                       -- !1
   ,[Out Zero,In One]               -- !0 | ?1
   ,[Out One,Out One,In Zero]       -- !1 | !1 | ?0
   ,[Out Zero,Out Zero,In One]      -- !0 | !0 | ?1
   ,[Out Zero,In One,In One]        -- !0 | ?1 | ?1
   ,[Out Zero,In One,In One,In One] -- !0 | ?1 | ?1 | ?1
   ]
criticalProcs = map PISIMPLEProc
  [[Out One,In Zero]                        -- !1 | ?0
  ,[Out Zero,In One]                        -- !0 | ?1
  ,[Out (In Zero)]                          -- !?0
  ,[Out Zero]                               -- !0
  ,[Out Zero,In One,In One]                 -- !0 | ?1 | ?1
  ,[Out One,Out One,In Zero]                -- !1 | !1 | ?0
  ,[Out (In One),In Zero]                   -- !?1 | ?0
  ,[In (Out One),Out Zero]                  -- ?!1 | !0
  ,[In (Out One),Out Zero,Out Zero]         -- ?!1 | !0 | !0
  ,[Out (In One),In Zero,In Zero]           -- !?1 | ?0 | ?0
  ,[Out (In One),Out (In One),In Zero]      -- !?1 | !?1 | ?0
  ,[Out One,Out One,Out One,In Zero]        -- !1 | !1 | !1 | ?0
  ,[Out Zero,In One,In One,In One]          -- !0 | ?1 | ?1 | ?1  
  ,[Out (In One)]                           -- !?1
  ,[Out One]                                -- !1
  ,[Out One,Out One,In Zero,In Zero]        -- !1 | !1 | ?0 | ?0
  ,[Out One,Out (Out One),In Zero,In Zero]  -- !1 | !!1 | ?0 | ?0
  ,[Out (Out One),In Zero]                  -- !!1 | ?0
  ,[Out One,In Zero,Out (In (Out Zero))]    -- !1 | ?0 | !?!0
  ,[Out Zero,In Zero,Out (In (Out One))]    -- !0 | ?0 | !?!1
   ]
calculated = 
   [[Out One]          -- !1
   ,[Out One, In Zero] -- !1 | ?0
   ,[Out (In One), In Zero] -- !?1 | ?0
   ,[Out Zero, In (Out Zero)] -- !0 | ?!1
   ,[Out (Out One), In Zero] -- !!1 | ?0
   ,[Out One, In Zero, In Zero] -- !1 | ?0 | ?0
   ,[Out Zero, Out One] -- !0 | !1
   ,[Out One, In (Out Zero)] -- !1 | ?!0
   ,[Out (In Zero), In One] -- !?0 | ?1
   ,[Out One, Out One, In Zero] -- !1 | !1 | ?0
   ,[Out One, Out One, In (Out Zero)] -- !1 | !1 | ?!0
   ,[Out Zero, Out (In (Out One)), In Zero] -- !0 | !?!1 | ?0
   ,[Out (Out One), In (In Zero)] -- !!1 | ??0
   ]

  
examplesInput =  (criticalProcs) ++ map PISIMPLEProc (reverse calculated)
exlist exdep = (examplesInput ++ (genSamples exdep) )

-- unfold the ^* and the Alternatives of a regular expression
unfoldPat :: CHSIMPLEProcess -> [CHSIMPLEProcess]
unfoldPat [] = [[]]
unfoldPat ((PTStar):ys)  = concat [[pat,PTPlus:pat] |  pat <- unfoldPat ys]
unfoldPat ((TPStar):ys)  = concat [[pat,TPPlus:pat] |  pat <- unfoldPat ys]
unfoldPat ((Alt xs) :ys) = [x ++ pat | a <- xs, x <- unfoldPat a, pat <- unfoldPat ys]
unfoldPat (y:ys) = [y:pat |  pat <- unfoldPat ys]

-- --------------------------------------------------------------------------------
-- interface to the regex library:

chSimpleActionToRE :: CHSimpleAction -> RE Char
chSimpleActionToRE Rec = L 'R'
chSimpleActionToRE Send = L 'S'
chSimpleActionToRE Take = L 'T'
chSimpleActionToRE Put = L 'P'
chSimpleActionToRE PTPlus = let pt = Seq (L 'P') (L 'T') in Seq pt (Star pt)
chSimpleActionToRE TPPlus = let tp = Seq (L 'T') (L 'P') in Seq tp (Star tp)
chSimpleActionToRE PTStar = let pt = Seq (L 'P') (L 'T') in (Star pt)
chSimpleActionToRE TPStar = let tp = Seq (L 'T') (L 'P') in (Star tp)
chSimpleActionToRE (Alt xs) = resToRE (map chSimpleProcessToRE xs)
chSimpleActionToRE More = Star (Choice (L 'P') (L 'T'))

chSimpleProcessToRE [] = Empty
chSimpleProcessToRE  (x:xs) = Seq (chSimpleActionToRE x) (chSimpleProcessToRE xs)

-- Check equality of and containment of Actions
checkEq e1 e2 = equality (chSimpleActionToRE e1) (chSimpleActionToRE e2)
checkIsContainedIn e1 e2 = contains (chSimpleActionToRE e1) (chSimpleActionToRE e2)

-- check whether one translation-set is a subset of another translation set  
subseteqTranslations l1 l2 =
  (foldl1 Choice [  (Seq (Seq (chSimpleProcessToRE s) (L '#')) (chSimpleProcessToRE r)) | (s,r) <- l1])
  `contains` 
  (foldl1 Choice [  (Seq (Seq (chSimpleProcessToRE s) (L '#')) (chSimpleProcessToRE r)) | (s,r) <- l2])

-- check equality of two translation sets.
equalTranslations t1 t2 = subseteqTranslations t1 t2 && subseteqTranslations t2 t1  

-- Short--cuts for expansions of patterns

-- Expansions of More
morePatExpansionLong    = Alt [[],[Put],[Take],[PTPlus],[PTPlus,Put],[PTPlus,Take,More],[PTPlus,Put,Put,More],[Take,More], [Put,Put,More]]
morePatExpansionShort = Alt [[],[Put,More],[Take,More]]

-- check the correctness of the pattern expansion
validExpansionLong  = checkEq More morePatExpansionLong
validExpansionShort = checkEq More morePatExpansionShort
 

------------------------------------------------------------------------------------------------------
-- Tests start here!

-- ===================================================================
-- Proposition PropSmallTranslations:
-- ===================================================================
propSmallTranslations = count 4 6 1 "valid"  -- refutes all translations with length no longer than 10 Take and Put-Operations


------------------------------------------------------------------------------------------------------
-- Lemmas proved by hand, and used in the following


{--

===================================================================
LemmaPPPTTT
===================================================================
 Let  t(!) =  (PT)^nSP^ks3  and $t(?) =  RT^hr3, where n \>= 0 , h, >= 2, h+k >= 5
 s3 does not start with P, and r3 does not start with T.
 Then the translation is not correct. 

-}

lemma_ppp_ttt :: [Translation] -> Bool -- check if the lemma refutes the given translations
lemma_ppp_ttt l =
  (foldl1 Choice [(Seq (Seq (chSimpleProcessToRE s) (L '#')) (chSimpleProcessToRE r)) | (s,r) <- l])
  `contains`
      (foldl1 Choice [
      foldl Seq Empty (  
            (map chSimpleActionToRE  [PTStar, Send])
        ++ [let p = ((chSimpleActionToRE Put)) in Seq p (Seq p (Star p))] 
        ++ (map chSimpleActionToRE [ Alt [[], [Take, More]]])
        ++ [L '#']
        ++ [chSimpleActionToRE  Rec] 
        ++ [let t = chSimpleActionToRE Take in Seq t (Seq t (Seq t (Star t)))] 
        ++ [chSimpleActionToRE $ Alt [[], [Put,More]]]
        ),
      foldl Seq Empty (  
            (map chSimpleActionToRE  [PTStar, Send])
        ++ [let p = ((chSimpleActionToRE Put)) in Seq p (Seq p (Seq p (Star p)))] 
        ++ (map chSimpleActionToRE [ Alt [[], [Take, More]]])
        ++ [L '#']
        ++ [chSimpleActionToRE  Rec] 
        ++ [let t = chSimpleActionToRE Take in Seq t (Seq t ((Star t)))] 
        ++ [chSimpleActionToRE $ Alt [[], [Put,More]]]
        )
        ]  
      )
{--

===================================================================
LemmaTTTPPP
===================================================================
 Let  $t(!) =  (PT)^nST^ks3$  and $t(?) =   RP^hr3$, where $n >= 0$, $h,k >= 2$,
 s3 does not start with $T$, 
 and r3 does not start with $P$.
 Then the translation is not correct. 

-}

lemma_ttt_ppp :: [Translation] -> Bool -- check if the lemma refutes the given translations
lemma_ttt_ppp l =
  (foldl1 Choice [(Seq (Seq (chSimpleProcessToRE s) (L '#')) (chSimpleProcessToRE r)) | (s,r) <- l])
  `contains`
      (foldl1 Choice [
      foldl Seq Empty (  
            (map chSimpleActionToRE  [PTStar, Send])
        ++ [let p = ((chSimpleActionToRE Take)) in Seq p (Seq p (Star p))] 
        ++ (map chSimpleActionToRE [ Alt [[], [Put, More]]])
        ++ [L '#']
        ++ [chSimpleActionToRE  Rec] 
        ++ [let t = chSimpleActionToRE Put in (Seq t (Seq t (Star t)))] 
        ++ [chSimpleActionToRE $ Alt [[], [Take,More]]]
        )
        ]  
      )      




-- -------------------------------------------------------------------------------------------
-- For the proof of Lemma TTTPPP the following checks are made
testTTPP = findUnref [([Send,Take,Take],[Rec,Put,Put,Put])] criticalProcs
testTTPPn n m = findUnref [([Send]++(take n (repeat Take)) ++ [Alt [[],[Put,More]]],[Rec]++(take m (repeat Put)) ++ [Alt [[],[Take,More]]])] criticalProcs

-- 
-- Checking the case k=2 and h=3:
case_23 = testTTPPn 2 3
-- remaing unrefuted translations
result_case_23 = [([Send,Take,Take],[Rec,Put,Put,Put,Take,More]),([Send,Take,Take,Put,More],[Rec,Put,Put,Put,Take,More])]
-- expansion
case_23_1        =  [([Send,Take,Take],[Rec,Put,Put,Put,Take,Alt [[],[Put,More],[Take,More]]]),([Send,Take,Take,Put,More],[Rec,Put,Put,Put,Take,Alt [[],[Put,More],[Take,More]]])] 
-- checking correctness of expansion
check_case_23_1  = subseteqTranslations result_case_23 case_23_1
-- testing expansion
test_case_23_1   = findUnref case_23_1 criticalProcs
-- remaing unrefuted translations
result_case_23_1 = [([Send,Take,Take],[Rec,Put,Put,Put,Take,Put,More]),([Send,Take,Take,Put,More],[Rec,Put,Put,Put,Take,Put,More])]
-- expansion
case_23_2        = [([Send,Take,Take],[Rec,Put,Put,Put,Take,Put,Alt [[],[Put,More],[Take,More]]]),([Send,Take,Take,Put,More],[Rec,Put,Put,Put,Take,Put,Alt [[],[Put,More],[Take,More]]])]
-- checking correctness of expansion
check_case_23_2  = subseteqTranslations result_case_23_1 case_23_2
-- testing expansionTPnT-PTnP-final 
test_case_23_2   = findUnref   case_23_2 criticalProcs
-- remaining unrefuted translations
result_case_23_2 = [result_case_23_21, result_case_23_22]
result_case_23_21  = ([Send,Take,Take],[Rec,Put,Put,Put,Take,Put,Take,More])
result_case_23_22 =  ([Send,Take,Take,Put,More],[Rec,Put,Put,Put,Take,Put,Take,More])
-- expansion of first case:
case_23_21_expand1 = [([Send,Take,Take],[Rec,Put,Put, Alt [[],[Put],[Take,Take,More], [PTPlus],[PTPlus,Take,More],[PTPlus,Put],[PTPlus,Put,Put,More]]])]
-- checking correctness of expansion
check_case_23_21_expand1 = subseteqTranslations  [result_case_23_21 ] case_23_21_expand1
-- testing expansion
test_case_23_21_expand1 = findUnref case_23_21_expand1 criticalProcs
                                                   -- result: all translations refuted                                                  
-- expansion of second case:
case_23_22_expand1 = [([Send,Take,Take, Put,More],[Rec,Put,Put,Alt [[],[Put],[Take,Take,More], [PTPlus],[PTPlus,Take,More],[PTPlus,Put],[PTPlus,Put,Put,More]]])]
-- checking correctness of expansion
check_case_23_22_expand1 = subseteqTranslations  [result_case_23_22 ] case_23_22_expand1
-- testing expansion
test_case_23_22_expand1 = findUnref case_23_22_expand1 criticalProcs
                                                   -- result: all translations refuted                                                  
-- -------------------------------------------------------------------------------------------
testSPP_TRT n m = findUnref [([Send]++(take n (repeat Put)) ++ [Alt [[],[Take,More]]],[Take,Rec]++(take m (repeat Take)) ++ [Alt [[],[Put,More]]])] criticalProcs
-- -------------------------------------------------------------------------------------------


-- =========================================
-- Proposition  PropPrefixAlternating  
--   case 1:   special cases for s1: the first double coccurence is formalized.
--   it cannot be solved automatically, since a T^k is needed and a counter example
--   process is !1|...|!1  with k+1 occurrences of !1.
case1OfPropPrefixAlternating =   [ ([PTStar,Alt [[Take,More],[Put,Put,More]]], [Take,More])  ]  
test_case1OfPropPrefixAlternating  = findUnref (case1OfPropPrefixAlternating) criticalProcsFlat     
-- unrefuted:
-- ([P,P,M],[T,M])
-- ([(PT)+,P,P,M],[T,M])
-- ==========================================


           
--  Lemma LemmaSPn-TRTm
-- ===================
--  There are no  correct translations of the form (SP^kM,TRT^hM)  for k,h >= 1.
lemma_SPM_TRTM :: [Translation] -> Bool -- check if the lemma refutes the given translations
lemma_SPM_TRTM l =
  (foldl1 Choice [(Seq (Seq (chSimpleProcessToRE s) (L '#')) (chSimpleProcessToRE r)) | (s,r) <- l])
  `contains`
      (foldl1 Choice [
      foldl Seq Empty (  
            (map chSimpleActionToRE  [Send])
        ++ [let p = ((chSimpleActionToRE Put)) in Seq p ((Star p))] 
        ++ (map chSimpleActionToRE [More])
        ++ [L '#']
        ++ (map chSimpleActionToRE  [Take,Rec])
        ++ [let t = chSimpleActionToRE Take in (Seq t (Star t))] 
        ++ [chSimpleActionToRE More]
        )
        ]  
      )      





-- ============================================================
-- Tests for LemmaTPnT-PTnP
-- ============================================================
-- case 1:
-- the case to be shown:
lemmaTPnT_PTnP_case1  = [([TPStar,Send,More], [TPStar,Take,Rec,More])]
-- expand the case
lemmaTPnT_PTnP_case1_expand1
           = [ ([TPPlus,Send,More],[TPStar,Take,Rec,More])
            ,([Send],[TPStar,Take,Rec,More])
            ,([Send,Put],[TPStar,Take,Rec,More])
            ,([Send,PTPlus],[TPStar,Take,Rec,More])
            ,([Send,PTPlus,Put],[TPStar,Take,Rec,More])
            ,([Send,PTPlus,Take,More],[TPStar,Take,Rec,More])
            ,([Send,PTPlus,Put,Put,More],[TPStar,Take,Rec,morePatExpansionShort])
            ,([Send,Take,More],[TPStar,Take,Rec,More])
            ,([Send,Put,Put,More],[TPStar,Take,Rec,More])  
             ]
-- correctness of expansion
check_lemmaTPnT_PTnP_case1_expand1 = subseteqTranslations lemmaTPnT_PTnP_case1 lemmaTPnT_PTnP_case1_expand1
-- testing expansion
test_lemmaTPnT_PTnP_case1_expand1 = findUnref lemmaTPnT_PTnP_case1_expand1 criticalProcs
-- remaining unrefuted translations
result_lemmaTPnT_PTnP_case1_expand1 = [([Send,Put,Put,More],[Take,Rec,More]),([Send,Put,Put,More],[TPPlus,Take,Rec,More])]
-- expanding unrefuted translations
lemmaTPnT_PTnP_case1_expand2 = [([Send,Put,Put,morePatExpansionLong],[Take,Rec,morePatExpansionShort]),([Send,Put,Put,morePatExpansionLong],[TPPlus,Take,Rec,morePatExpansionShort])]
-- correctness of expansion
check_lemmaTPnT_PTnP_case1_expand2 = subseteqTranslations result_lemmaTPnT_PTnP_case1_expand1 lemmaTPnT_PTnP_case1_expand2
-- testing expansion
test_lemmaTPnT_PTnP_case1_expand2 = findUnref lemmaTPnT_PTnP_case1_expand2 criticalProcs
-- remaining unrefuted translations
result_lemmaTPnT_PTnP_case1_expand2  =  [([Send,Put,Put,Put,Put,More],[Take,Rec,Take,More])]
 -- solved by hand-proved lemma:
solve_result_lemmaTPnT_PTnP_case1_expand2  =  lemma_SPM_TRTM result_lemmaTPnT_PTnP_case1_expand2


-- ============================================================
-- Tests for LemmaTPnT-PTnP
-- ============================================================
--   case 2:

lemmaTPnT_PTnP_case2         = [([PTStar,Send,More], [TPStar,Take,Rec,More])]
test_lemmaTPnT_PTnP_case2    = findUnref lemmaTPnT_PTnP_case2 criticalProcs
result_lemmaTPnT_PTnP_case2  = [([Send,More],[Take,Rec,More]),([Send,More],[TPPlus,Take,Rec,More])]
lemmaTPnT_PTnP_case2_expand1 = [([Send,morePatExpansionLong],[Take,Rec,morePatExpansionShort]),
                                ([Send,morePatExpansionLong],[TPPlus,Take,Rec,morePatExpansionShort])] 
check_lemmaTPnT_PTnP_case2_expand1  = subseteqTranslations  result_lemmaTPnT_PTnP_case2 lemmaTPnT_PTnP_case2_expand1
test_lemmaTPnT_PTnP_case2_expand1   = findUnref lemmaTPnT_PTnP_case2_expand1 criticalProcs
result_lemmaTPnT_PTnP_case2_expand1 =  [([Send,Put,Put,More],[Take,Rec,Take,More]),([Send,Put,Put,More],[TPPlus,Take,Rec,Take,More])]
lemmaTPnT_PTnP_case2_expand2 = [([Send,Put,Put,morePatExpansionLong],[Take,Rec,Take,morePatExpansionLong]),([Send,Put,Put,morePatExpansionLong],[TPPlus,Take,Rec,Take,morePatExpansionShort])]
check_lemmaTPnT_PTnP_case2_expand2 = subseteqTranslations result_lemmaTPnT_PTnP_case2_expand1 lemmaTPnT_PTnP_case2_expand2
test_lemmaTPnT_PTnP_case2_expand2 = findUnref lemmaTPnT_PTnP_case2_expand2 criticalProcs
result_lemmaTPnT_PTnP_case2_expand2 = [([Send,Put,Put,Put,Put,More],[Take,Rec,Take,Take,More])]
solve_result_lemmaTPnT_PTnP_case2_expand2 = lemma_SPM_TRTM result_lemmaTPnT_PTnP_case2_expand2
-- solvable by hand-proved lemma



-- ===================================================================
-- Proposition PTStarSMAndPTStarRTM   (PT)^*S... vs (PT)^*RT.... 
-- ===================================================================
--  In the case t(!) =  (PT)^nSs2  and t(?) =  (PT)^mRr2, the string r2 does not start with T.  

casePTStarSMAndPTStarRTM = [([PTStar,Send,More],[PTStar,Rec,Take,More])]

casePTStarSMAndPTStarRTM_expand1 = 
    [ ([PTStar,Send,morePatExpansionLong], [PTStar,Rec,Take,More]) 
    ]  
check_casePTStarSMAndPTStarRTM_expand1 = subseteqTranslations casePTStarSMAndPTStarRTM  casePTStarSMAndPTStarRTM_expand1  

test_casePTStarSMAndPTStarRTM_expand1  = findUnref (casePTStarSMAndPTStarRTM_expand1) criticalProcsFlat     
result_casePTStarSMAndPTStarRTM_expand1 = [([Send,Put,Put,More],[Rec,Take,More]),([Send,PTPlus,Put,Put,More],[Rec,Take,More]),([PTPlus,Send,Put,Put,More],[Rec,Take,More]),([PTPlus,Send,PTPlus,Put,Put,More],[Rec,Take,More])]
casePTStarSMAndPTStarRTM_expand2 = 
    [ ([PTStar,Send,Alt [[PTStar],[PTStar,Put],[PTStar,Take,More],[PTStar,Put,Put,More]]], [PTStar,Rec,Take,More]) 
    ]  
check_casePTStarSMAndPTStarRTM_expand2 = subseteqTranslations result_casePTStarSMAndPTStarRTM_expand1 casePTStarSMAndPTStarRTM_expand2
test_casePTStarSMAndPTStarRTM_expand2  = findUnref (casePTStarSMAndPTStarRTM_expand2) criticalProcsFlat     
result_casePTStarSMAndPTStarRTM_expand2 = [([Send,Put,Put,More],[Rec,Take,More]),([Send,PTPlus,Put,Put,More],[Rec,Take,More]),([PTPlus,Send,Put,Put,More],[Rec,Take,More]),([PTPlus,Send,PTPlus,Put,Put,More],[Rec,Take,More])]

casePTStarSMAndPTStarRTM_expand3 = [([PTPlus,Send,PTPlus,Put,Put,More],[Rec,Take,Alt [[],[Take,More],[Put,More]  ]]), 
    ([PTPlus,Send,Put,Put,More],[Rec,Take,Alt [[],[Take,More],[Put,More] ]]),
    ([Send,PTPlus,Put,Put,More],[Rec,Take,Alt [[],[Take,More],[Put,More]  ]]),
    ([Send,Put,Put,More],[Rec,Take,Alt [[],[Take,More],[Put,More]  ]])]

check_casePTStarSMAndPTStarRTM_expand3 = subseteqTranslations result_casePTStarSMAndPTStarRTM_expand2 casePTStarSMAndPTStarRTM_expand3
test_casePTStarSMAndPTStarRTM_expand3 = findUnref casePTStarSMAndPTStarRTM_expand3 criticalProcs
result_casePTStarSMAndPTStarRTM_expand3 =  [([PTPlus,Send,Put,Put,More],[Rec,Take,Take,More]),([Send,Put,Put,More],[Rec,Take,Take,More])]  
casePTStarSMAndPTStarRTM_expand4 = [([PTPlus,Send,Put,Put,Alt [[],[Take,More], [Put,More]]],[Rec,Take,Take,Alt [[],[Take,More], [Put,More]]]),
           ([Send,Put,Put,Alt [[],[Take,More], [Put,More]]],[Rec,Take,Take,Alt [[],[Take,More], [Put,More]]])]
check_casePTStarSMAndPTStarRTM_expand4 = subseteqTranslations result_casePTStarSMAndPTStarRTM_expand3 casePTStarSMAndPTStarRTM_expand4
test_casePTStarSMAndPTStarRTM_expand4 =  findUnref casePTStarSMAndPTStarRTM_expand4 criticalProcs
result_casePTStarSMAndPTStarRTM_expand4 = [ case1,case2,case3,case4,case5,case6 ] -- cases are below

case1                 = ([PTPlus,Send,Put,Put],[Rec,Take,Take,Put,More])
patcase1              = [([PTPlus,Send,Put,Put],[Rec,Take,TPPlus, Alt [[],[Put,More],[Take],[Take,Take,More]]])]
testCompletenessCase1 = subseteqTranslations [case1] patcase1
solvePatCase1         = findUnref patcase1 criticalProcs  -- result: all translations refuted

case2                 = ([PTPlus,Send,Put,Put,Take,More],[Rec,Take,Take,Put,More])
patcase2              = [([PTPlus,Send,Put,Put,Take,More],[Rec,Take,TPPlus,Alt [[],[Put,More],[Take],[Take,Take,More]]])]
testCompletenessCase2 = subseteqTranslations [case2] patcase2
solvePatCase2         = findUnref patcase2 criticalProcs  -- result: all translations refuted

case3                 = ([PTPlus,Send,Put,Put,Put,More],[Rec,Take,Take,Take,More])
 -- translations refutable by hand-made proof
solveCase3 = lemma_ppp_ttt [case3]

case4                 = ([Send,Put,Put],[Rec,Take,Take,Put,More])
patcase4              = [([Send,Put,Put],[Rec,Take,Take,PTStar,Alt [[],[Put],[Put,Put,More],[Take,More]  ] ])] 
testCompletenessCase4 = subseteqTranslations [case4] patcase4
solvePatCase4         = findUnref patcase4 criticalProcs -- result: all translations refuted

case5                 = ([Send,Put,Put,Take,More],[Rec,Take,Take,Put,More])
patcase5 = [([Send,Put,Put,Take,More],[Rec,Take,TPPlus,Alt [[],[Put,More],[Take],[Take,Take,More]]])]
testCompletenessCase5 = subseteqTranslations [case5] patcase5
solvePatCase5         = findUnref patcase5  criticalProcs -- result: all translations refuted

case6                 = ([Send,Put,Put,Put,More],[Rec,Take,Take,Take,More])
solveCase6             = lemma_ppp_ttt [case6] 
 -- translations refutable by hand-made proof



 
 
 
-- Proposition: PropPTStarANDPTStarRP   ( PT*S...  vs     PT* RP... )  
-- ==================================
--  In the case t(!) =  (PT)^nSs2  and t(?) =  (PT)^mRr_2 the string r_2 is nonempty and does not start with $P$.  
-- =====================
-- Start with the translations:
case_PTStarANDPTStarRP =  [ ([PTStar,Send,More], [PTStar,Rec,Put,More])  ]  
-- We exand the pattern:
case_PTStarANDPTStarRP_expand1        = [ ([PTStar,Send,Alt [[TPStar],[TPStar,Take],[TPStar,Put,More],[TPStar,Take,Take,More]]], [PTStar,Rec,Put,More])  ]  
-- Correctness of the expansions:
check_case_PTStarANDPTStarRP_expand1  = equalTranslations case_PTStarANDPTStarRP case_PTStarANDPTStarRP_expand1
-- Checking the translations
test_case_PTStarANDPTStarRP_expand1   = findUnref case_PTStarANDPTStarRP_expand1 criticalProcsFlat 
-- results in the following unrefuted translations:
result_case_PTStarANDPTStarRP_expand1 = [([Send,Take],[Rec,Put,More]),([Send,Take,Take,More],[Rec,Put,More]),([Send,Put,More],[Rec,Put,More]),([Send,Put,More],[PTPlus,Rec,Put,More]),([Send,TPPlus],[Rec,Put,More]),([Send,TPPlus],[PTPlus,Rec,Put,More]),([Send,TPPlus,Take],[Rec,Put,More]),([Send,TPPlus,Take,Take,More],[Rec,Put,More]),([Send,TPPlus,Put,More],[Rec,Put,More]),([Send,TPPlus,Put,More],[PTPlus,Rec,Put,More]),([PTPlus,Send,Take],[Rec,Put,More]),([PTPlus,Send,Take,Take,More],[Rec,Put,More]),([PTPlus,Send,Put,More],[Rec,Put,More]),([PTPlus,Send,Put,More],[PTPlus,Rec,Put,More]),([PTPlus,Send,TPPlus],[Rec,Put,More]),([PTPlus,Send,TPPlus],[PTPlus,Rec,Put,More]),([PTPlus,Send,TPPlus,Take],[Rec,Put,More]),([PTPlus,Send,TPPlus,Take,Take,More],[Rec,Put,More]),([PTPlus,Send,TPPlus,Put,More],[Rec,Put,More]),([PTPlus,Send,TPPlus,Put,More],[PTPlus,Rec,Put,More])]
-- we expand the result
case_PTStarANDPTStarRP_expand2        = [ ([PTStar,Send,Alt [[PTStar],[PTStar,Put],[PTStar,Take,More],[PTStar,Put,Put,More]]], [PTStar,Rec,Put,More])  ]  
-- checking correctness of the expansion:
check_case_PTStarANDPTStarRP_expand2  = subseteqTranslations case_PTStarANDPTStarRP_expand1 case_PTStarANDPTStarRP_expand2
-- checking, the translations
test_case_PTStarANDPTStarRP_expand2   =  findUnref case_PTStarANDPTStarRP_expand2  criticalProcsFlat
-- results in 8 cases:
result_case_PTStarANDPTStarRP_expand2  = 
             [result_case_PTStarANDPTStarRP_expand2_1
             ,result_case_PTStarANDPTStarRP_expand2_2
             ,result_case_PTStarANDPTStarRP_expand2_3
             ,result_case_PTStarANDPTStarRP_expand2_4
             ,result_case_PTStarANDPTStarRP_expand2_5
             ,result_case_PTStarANDPTStarRP_expand2_6
             ,result_case_PTStarANDPTStarRP_expand2_7
             ,result_case_PTStarANDPTStarRP_expand2_8]
result_case_PTStarANDPTStarRP_expand2_1 = ([PTPlus,Send,PTPlus,Take,More],[PTPlus,Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_2 = ([PTPlus,Send,PTPlus,Take,More],[Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_3 = ([PTPlus,Send,Take,More],       [PTPlus,Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_4 = ([PTPlus,Send,Take,More],[Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_5 = ([Send,PTPlus,Take,More],[PTPlus,Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_6 = ([Send,PTPlus,Take,More],[Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_7 = ([Send,Take,More],[PTPlus,Rec,Put,More])
result_case_PTStarANDPTStarRP_expand2_8 = ([Send,Take,More],[Rec,Put,More]) 


-- Solving result_case_PTStarANDPTStarRP_expand2_1
case_PTStarANDPTStarRP_expand2_1_expand_1        = [([PTPlus,Send,PTPlus,Take,More],[PTPlus,Rec,Put,Alt [[],[Put,More],[Take,More]]])]
check_case_PTStarANDPTStarRP_expand2_1_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_1] case_PTStarANDPTStarRP_expand2_1_expand_1
test_case_PTStarANDPTStarRP_expand2_1_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_1_expand_1 criticalProcsFlat
result_case_PTStarANDPTStarRP_expand2_1_expand_1 = [([PTPlus,Send,PTPlus,Take,More],[PTPlus,Rec,Put,Take,More])]
case_PTStarANDPTStarRP_expand2_1_expand_2        = [([PTPlus,Send,PTPlus,Take,More],[PTPlus,Rec,PTPlus,Alt [[],[Put],[Put,Put,More],[Take,More]]])] 
check_case_PTStarANDPTStarRP_expand2_1_expand_2  = subseteqTranslations result_case_PTStarANDPTStarRP_expand2_1_expand_1 case_PTStarANDPTStarRP_expand2_1_expand_2
test_case_PTStarANDPTStarRP_expand2_1_expand_2   = findUnref case_PTStarANDPTStarRP_expand2_1_expand_2 criticalProcs  
                                                   -- result: all translations refuted


-- Solving result_case_PTStarANDPTStarRP_expand2_2
case_PTStarANDPTStarRP_expand2_2_expand_1        = [([PTPlus,Send,PTPlus,Take,More], [Alt [[Rec,Put],[Rec,Put,Put,More], [Rec, PTPlus, Alt [[],[Put],[Put,Put,More],[Take,More]]]]])]  
check_case_PTStarANDPTStarRP_expand2_2_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_2] case_PTStarANDPTStarRP_expand2_2_expand_1
-- $> check_case_PTStarANDPTStarRP_expand2_2_expand_1           
test_case_PTStarANDPTStarRP_expand2_2_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_2_expand_1 criticalProcs
                                                   -- result: all translations refuted                                                  
                                                   

-- Solving result_case_PTStarANDPTStarRP_expand2_3
case_PTStarANDPTStarRP_expand2_3_expand_1        = [([PTPlus,Send,Alt [[Take],[Take,Take,More],[TPPlus],[TPPlus,Put,More],[TPPlus,Take],[TPPlus,Take,Take,More]]], 
                                                        [PTPlus,Rec, Alt [[Put],[Put,Put,More], [PTPlus, Alt [[],[Put],[Put,Put,More],[Take,More]]]]])] 
check_case_PTStarANDPTStarRP_expand2_3_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_3] case_PTStarANDPTStarRP_expand2_3_expand_1
test_case_PTStarANDPTStarRP_expand2_3_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_3_expand_1 criticalProcs
                                                   -- result: all translations refuted                                                  

-- Solving result_case_PTStarANDPTStarRP_expand2_4
case_PTStarANDPTStarRP_expand2_4_expand_1        =  [([PTPlus,Send,Alt [[Take],[Take,Take,More],[TPPlus],[TPPlus,Put,More],[TPPlus,Take],[TPPlus,Take,Take,More]]], 
                                                          [Alt [[Rec,Put],[Rec,Put,Put,Alt [[],[Put,More],[Take,More]]], [Rec, PTPlus, Alt [[],[Put],[Put,Put,More],[Take,More]]]]])]  
check_case_PTStarANDPTStarRP_expand2_4_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_4] case_PTStarANDPTStarRP_expand2_4_expand_1
test_case_PTStarANDPTStarRP_expand2_4_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_4_expand_1 criticalProcs
result_case_PTStarANDPTStarRP_expand2_4_expand_1 = [([PTPlus,Send,Take,Take,More],[Rec,Put,Put,Put,More])]
solve_case_PTStarANDPTStarRP_expand2_4_expand_1  = lemma_ttt_ppp [([PTPlus,Send,Take,Take,More],[Rec,Put,Put,Put,More])]

  
  
   
 
  
-- Solving result_case_PTStarANDPTStarRP_expand2_5
case_PTStarANDPTStarRP_expand2_5_expand_1        =  [([Send,PTPlus,Take,More], [PTPlus,Rec,Alt [[Put],[Put,Put,More],[PTPlus], [PTPlus,Put], [PTPlus,Put,Put,More],[PTPlus,Take,More]]   ])] 
check_case_PTStarANDPTStarRP_expand2_5_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_5] case_PTStarANDPTStarRP_expand2_5_expand_1
-- True
test_case_PTStarANDPTStarRP_expand2_5_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_5_expand_1 criticalProcs
                                                   -- result: all translations refuted


-- Solving result_case_PTStarANDPTStarRP_expand2_6
case_PTStarANDPTStarRP_expand2_6_expand_1        = [([Send,PTPlus,Take,More], [Rec,Alt [[Put],[Put,Put,More],[PTPlus], [PTPlus,Put], [PTPlus,Put,Put,More],[PTPlus,Take,More]]   ])]  
check_case_PTStarANDPTStarRP_expand2_6_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_6] case_PTStarANDPTStarRP_expand2_6_expand_1
-- True
test_case_PTStarANDPTStarRP_expand2_6_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_6_expand_1 criticalProcs
                                                   -- result: all translations refuted


-- Solving result_case_PTStarANDPTStarRP_expand2_7
case_PTStarANDPTStarRP_expand2_7_expand_1        = [([Send,Alt [[Take],[Take,Take,More],[TPPlus],[TPPlus,Take], [TPPlus,Take,Take,More],[TPPlus,Put,More]]], [PTPlus,Rec,Alt [[Put],[Put,Put,More],[PTPlus], [PTPlus,Put], [PTPlus,Put,Put,More],[PTPlus,Take,More]]])] 
check_case_PTStarANDPTStarRP_expand2_7_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_7] case_PTStarANDPTStarRP_expand2_7_expand_1
-- True
test_case_PTStarANDPTStarRP_expand2_7_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_7_expand_1 criticalProcs
                                                   -- result: all translations refuted


-- Solving result_case_PTStarANDPTStarRP_expand2_8
case_PTStarANDPTStarRP_expand2_8_expand_1        = [([Send,Alt [[Take],[Take,Take,More],[TPPlus],[TPPlus,Take], [TPPlus,Take,Take,More],[TPPlus,Put,More]]  ] ,  [Rec,Alt [[Put],[Put,Put,More],[PTPlus], [PTPlus,Put], [PTPlus,Put,Put,More],[PTPlus,Take,More]]   ])]
-- True
check_case_PTStarANDPTStarRP_expand2_8_expand_1  = subseteqTranslations [result_case_PTStarANDPTStarRP_expand2_8] case_PTStarANDPTStarRP_expand2_8_expand_1
test_case_PTStarANDPTStarRP_expand2_8_expand_1   = findUnref case_PTStarANDPTStarRP_expand2_8_expand_1 criticalProcs
result_case_PTStarANDPTStarRP_expand2_8_expand_1 = [([Send,Take,Take,More],[Rec,Put,Put,More]),([Send,TPPlus,Take,Take,More],[Rec,Put,Put,More])]
case_PTStarANDPTStarRP_expand2_8_expand_2a = [([Send,Take,Take,Alt [[],[Take,More],[Put,More] ]],[Rec,Put,Put,Alt [[],[Take,More],[Put,More] ]])]
case_PTStarANDPTStarRP_expand2_8_expand_2b = [([Send,TPPlus,Take,Take,Alt [[],[Take,More],[Put,More] ]],[Rec,Put,Put,Alt [[],[Take,More],[Put,More] ]])]
case_PTStarANDPTStarRP_expand2_8_expand_2 = case_PTStarANDPTStarRP_expand2_8_expand_2a ++ case_PTStarANDPTStarRP_expand2_8_expand_2b
check_case_PTStarANDPTStarRP_expand2_8_expand_2  = subseteqTranslations result_case_PTStarANDPTStarRP_expand2_8_expand_1 case_PTStarANDPTStarRP_expand2_8_expand_2
test_case_PTStarANDPTStarRP_expand2_8_expand_2a  = findUnref case_PTStarANDPTStarRP_expand2_8_expand_2a criticalProcs
result_case_PTStarANDPTStarRP_expand2_8_expand_2a  = [([Send,Take,Take],[Rec,Put,Put,Put,More]),([Send,Take,Take,Take,More],[Rec,Put,Put,Put,More]),([Send,Take,Take,Put,More],[Rec,Put,Put,Put,More])]
          --        refutable by hand using LemmaTTTPPP
solve_result_case_PTStarANDPTStarRP_expand2_8_expand_2a=
   lemma_ttt_ppp result_case_PTStarANDPTStarRP_expand2_8_expand_2a
test_case_PTStarANDPTStarRP_expand2_8_expand_2b = findUnref case_PTStarANDPTStarRP_expand2_8_expand_2b criticalProcs
                                                   -- result: all translations refuted


-- case of PropPTStarAndPTStarRP: if the string r2 is empty:
case_PTStarANDPTStarRP_r2Empty =  [ ([PTStar,Send,Alt [[PTStar],[PTStar,Put],[PTStar,Take,More],[PTStar,Put,Put,More]]], [PTStar,Rec])  ]    
test_case_PTStarANDPTStarRP_r2Empty = findUnref case_PTStarANDPTStarRP_r2Empty criticalProcs
                                                   -- result: all translations refuted


-- ===============================================================
-- Proposition PropPT-TP-Prefix  (1)    PT*S... vs TP*RP...  
-- ===============================================================


case_PTStarANDTPStarRP                = [ ([PTStar,Send,More], [TPStar,Rec,Put,More])  ] 
case_PTStarANDTPStarRP_expand1        = [ ([PTStar,Send,morePatExpansionLong],[TPStar,Rec,Put,More])  ] 
check_case_PTStarANDTPStarRP_expand1  = equalTranslations case_PTStarANDTPStarRP case_PTStarANDTPStarRP_expand1
test_case_PTStarANDTPStarRP_expand1   = findUnref case_PTStarANDTPStarRP_expand1 criticalProcs 
result_ccase_PTStarANDTPStarRP_expand1  = [
                                          case_PTStarANDTPStarRP_expand1_1,
                                          case_PTStarANDTPStarRP_expand1_2,
                                          case_PTStarANDTPStarRP_expand1_3,
                                          case_PTStarANDTPStarRP_expand1_4,
                                          case_PTStarANDTPStarRP_expand1_5,
                                          case_PTStarANDTPStarRP_expand1_6
                                          ]
case_PTStarANDTPStarRP_expand1_1 = ([PTPlus,Send,Take],[Rec,Put,More])
case_PTStarANDTPStarRP_expand1_2 = ([PTPlus,Send,PTPlus,Take,More],[Rec,Put,More])
case_PTStarANDTPStarRP_expand1_3 = ([Send,Take],[Rec,Put,More])
case_PTStarANDTPStarRP_expand1_4 = ([Send,PTPlus,Take,More],[Rec,Put,More])
-- neue Faelle:
case_PTStarANDTPStarRP_expand1_5 =([Send,Take,More],[Rec,Put,More])
case_PTStarANDTPStarRP_expand1_6 = ([PTPlus,Send,Take,More],[Rec,Put,More])


-- Solving case_PTStarANDTPStarRP_expand1_1
case_PTStarANDTPStarRP_expand1_1_expand1 = [([PTPlus,Send,Take],[Rec,Put,Alt [[],[Put,More],[Take,More] ]  ]) ] 
check_case_PTStarANDTPStarRP_expand1_1_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_1] case_PTStarANDTPStarRP_expand1_1_expand1
test_case_PTStarANDTPStarRP_expand1_1_expand1 = findUnref case_PTStarANDTPStarRP_expand1_1_expand1 criticalProcs
                                                   -- result: all translations refuted


-- Solving case_PTStarANDTPStarRP_expand1_2
case_PTStarANDTPStarRP_expand1_2_expand1 = [([PTPlus,Send,PTPlus,Take,More],[Rec,Alt [[Put],[Put, Put,More],[PTPlus],[PTPlus,Put],[PTPlus,Put,Put,More],[PTPlus,Take,More]]  ]) ] 
check_case_PTStarANDTPStarRP_expand1_2_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_2] case_PTStarANDTPStarRP_expand1_2_expand1
test_case_PTStarANDTPStarRP_expand1_2_expand1 = findUnref case_PTStarANDTPStarRP_expand1_2_expand1 criticalProcs
                                                   -- result: all translations refuted

-- Solving case_PTStarANDTPStarRP_expand1_3
case_PTStarANDTPStarRP_expand1_3_expand1 = [([Send,Take],[Rec,Put,Alt [[],[Put,More],[Take,More] ]  ]) ]
check_case_PTStarANDTPStarRP_expand1_3_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_3] case_PTStarANDTPStarRP_expand1_3_expand1
test_case_PTStarANDTPStarRP_expand1_3_expand1 = findUnref case_PTStarANDTPStarRP_expand1_3_expand1 criticalProcs
                                                   -- result: all translations refuted
-- Solving case_PTStarANDTPStarRP_expand1_4
case_PTStarANDTPStarRP_expand1_4_expand1 = [([Send,PTPlus,Take,More],[Rec,Alt [[Put],[Put, Put,More],[PTPlus],[PTPlus,Put],[PTPlus,Put,Put,More],[PTPlus,Take,More]]  ]) ] 
check_case_PTStarANDTPStarRP_expand1_4_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_4] case_PTStarANDTPStarRP_expand1_4_expand1
test_case_PTStarANDTPStarRP_expand1_4_expand1 = findUnref case_PTStarANDTPStarRP_expand1_4_expand1 criticalProcs
                                                   -- result: all translations refuted

-- Solving case_PTStarANDTPStarRP_expand1_5
--
-- case_PTStarANDTPStarRP_expand1_5 =([Send,Take,More],[Rec,Put,More])

case_PTStarANDTPStarRP_expand1_5_expand1 = case_PTStarANDPTStarRP_expand2_8_expand_1 
check_case_PTStarANDTPStarRP_expand1_5_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_5] case_PTStarANDTPStarRP_expand1_5_expand1

-- solved since case_PTStarANDPTStarRP_expand2_8_expand_1 is solved above
-- repetition of proof omitted

-- Solving case_PTStarANDTPStarRP_expand1_6
--
-- case_PTStarANDTPStarRP_expand1_6 = ([PTPlus,Send,Take,More],[Rec,Put,More])
case_PTStarANDTPStarRP_expand1_6_expand1 = [result_case_PTStarANDPTStarRP_expand2_4] 
check_case_PTStarANDTPStarRP_expand1_6_expand1 = subseteqTranslations [case_PTStarANDTPStarRP_expand1_6] case_PTStarANDTPStarRP_expand1_6_expand1
-- solved since result_case_PTStarANDPTStarRP_expand2_4 is solved above
-- repetition of proof omitted



-- ===============================================================
-- Proposition PropPT-TP-Prefix  (2)    TP*S... vs PT*R...   
-- ================================================================


case_TPStarANDPTStarRP =  [ ([TPStar,Send,More], [PTStar,Rec,More])  ] 
case_TPStarANDPTStarRPa =  [ ([TPStar,Send,More], [PTStar,Rec,Take,More])  ] 
case_TPStarANDPTStarRPb =  [ ([TPStar,Send,More], [PTStar,Rec,Put,More])  ] 
case_TPStarANDPTStarRPc =  [ ([TPStar,Send,More], [PTStar,Rec])  ] 
check_case_TPStarANDPTStarRPabc = subseteqTranslations case_TPStarANDPTStarRP (case_TPStarANDPTStarRPa ++ case_TPStarANDPTStarRPb ++ case_TPStarANDPTStarRPc) 


-- Solving case_TPStarANDPTStarRPa
case_TPStarANDPTStarRPa_expand1neu        = [ ([TPStar,Send, morePatExpansionLong], [PTStar,Rec,Take,More]) ]
check_case_TPStarANDPTStarRPa_expand1neu  = subseteqTranslations case_TPStarANDPTStarRPa case_TPStarANDPTStarRPa_expand1neu
test_case_TPStarANDPTStarRPa_expand1neu   = findUnref case_TPStarANDPTStarRPa_expand1neu criticalProcs
result_case_TPStarANDPTStarRPa_expand1neu = [([Send,Put,Put,More],[Rec,Take,More]),([Send,PTPlus,Put,Put,More],[Rec,Take,More])]
solve_case_TPStarANDPTStarRPa_expand1neu  = subseteqTranslations result_case_TPStarANDPTStarRPa_expand1neu result_casePTStarSMAndPTStarRTM_expand2
-- result_casePTStarSMAndPTStarRTM_expand2 is already solved, so there is nothing more to show.


-- Solving case_TPStarANDPTStarRPb
case_TPStarANDPTStarRPb_expand1neu = [ ([TPStar,Send,morePatExpansionLong], [PTStar,Rec,Put,More])  ] 
check_case_TPStarANDPTStarRPb_expand1neu  = subseteqTranslations case_TPStarANDPTStarRPb case_TPStarANDPTStarRPb_expand1neu
test_case_TPStarANDPTStarRPb_expand1neu   = findUnref case_TPStarANDPTStarRPb_expand1neu criticalProcs
result_case_TPStarANDPTStarRPb_expand1neu = [([Send,Take],[Rec,Put,More]),([Send,Take,More],[Rec,Put,More]),([Send,Take,More],[PTPlus,Rec,Put,More]),([Send,PTPlus,Take,More],[Rec,Put,More]),([Send,PTPlus,Take,More],[PTPlus,Rec,Put,More])]
solve_case_TPStarANDPTStarRPb_expand1neu  = subseteqTranslations result_case_TPStarANDPTStarRPb_expand1neu case_PTStarANDPTStarRP
  
-- case_PTStarANDPTStarRP is already solved above

-- Solving case_TPStarANDPTStarRPc
case_TPStarANDPTStarRPc_expand1 = [ ([TPStar,Send,morePatExpansionLong], [PTStar,Rec])  ]
check_TPStarANDPTStarRPc_expand1 = subseteqTranslations case_TPStarANDPTStarRPc case_TPStarANDPTStarRPc_expand1
test_case_TPStarANDPTStarRPc_expand1 = findUnref case_TPStarANDPTStarRPc_expand1 criticalProcs
                                                   -- result: all translations refuted


